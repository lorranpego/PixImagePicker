![](media/two.png)

# Pix   (WhatsApp Style Image Picker) used on Pink T-rano APP

Pix is a Whatsapp image picker replica.

With this you can integrate a image picker just like whatsapp.

Inspired by: [Akshay2211-PixImagePicker](https://github.com/akshay2211/PixImagePicker)

[![](https://img.shields.io/badge/API-16%2B-orange.svg?style=flat-square)](https://android-arsenal.com/api?level=16)


## Demo

![](media/media.gif)
![](media/one.png)

## Usage
 
```groovy
          Pix.start(Context,                    //Activity or Fragment Instance
                    RequestCode,                //Request code for activity results
                    NumberOfImagesToSelect);    //Number of images to restict selection count
```
or just use
```groovy
          Pix.start(Context,
                    RequestCode);
```
for fetching only a single picture.

Use onActivityResult method to get results
```groovy
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
                if (resultCode == Activity.RESULT_OK && requestCode == RequestCode) {
                    ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
            }
        }
```
## Customise
### Theme
include these items in colors.xml with custom color codes
```xml
    <resources>
        <color name="colorPrimaryPix">#075e54</color>
        <color name="colorPrimaryLightPix">#80075e54</color>
    </resources>
```

### Orientaion
include this theme in style.xml with preferred screen orientation
```xml
   <style name="PixAppTheme" parent="Theme.AppCompat.Light.NoActionBar">
         <item name="android:screenOrientation">portrait</item>
   </style>
```
## Permission Handling
include onRequestPermissionsResult method in your Activity/Fragment for permission selection
```groovy
   @Override
   public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
           switch (requestCode) {
               case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                   // If request is cancelled, the result arrays are empty.
                   if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                       Pix.start(Context, RequestCode,NumberOfImagesToSelect);
                    } else {
                       Toast.makeText(MainActivity.this, "Approve permissions to open Pix ImagePicker", Toast.LENGTH_LONG).show();
                   }
                   return;
               }
           }
       }
```

## Thanks to

  - [Glide]
  - [FastScroll]
  - [Header-decor]
  - [Fotoapparat]


Snapshots of the development version are available in [Sonatype's `snapshots` repository][snap].



## License
Licensed under the Apache License, Version 2.0, [click here for the full license](/LICENSE).
